""" Serializers for relationship application """
from rest_framework import serializers
from relationship.models import Relationship
from rest_framework.fields import SerializerMethodField
from core.serializers import BasicUserSerializer
from core.models import Notification
from core.utils import get_user_devices, send_push_notification,\
    get_image_of_user

class RelationshipSerializer(serializers.ModelSerializer):
    """ Relationship Serializer """
    obj_follower = SerializerMethodField()
    obj_followed = SerializerMethodField()

    class Meta:
        """ Meta class """
        model = Relationship
        fields = ['id', 'follower', 'followed', 'obj_follower', 'obj_followed']

    def create(self, validated_data):
        """ Create method """
        relationship = Relationship.objects.create(**validated_data)
        # Notification New Follower
        Notification.objects.create(user=relationship.followed,
                                    object_id=relationship.id, use_case=1)
        # Push notification
        devices = get_user_devices(relationship.followed.id)
        title = 'TuMentor'
        content = (relationship.follower.get_full_name() +
                   ' ha comenzado a seguirte')
        image = get_image_of_user(relationship.follower)
        push_data = {"use_case": 1, "id_follower": relationship.follower.id}
        send_push_notification(devices, title, content, image, push_data)
        return relationship

    def get_obj_follower(self, obj):
        """ Serialized follower """
        context = {'request': self.context['request']}
        obj_follower = BasicUserSerializer(obj.follower, context=context).data
        return obj_follower

    def get_obj_followed(self, obj):
        """ Serialized followed """
        context = {'request': self.context['request']}
        obj_followed = BasicUserSerializer(obj.followed, context=context).data
        return obj_followed
