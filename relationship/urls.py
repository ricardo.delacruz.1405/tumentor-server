""" URL's for relationship application """
from django.urls import path
from relationship.views import RelationshipViewSet

RELATIONSHIP_LIST = RelationshipViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
RELATIONSHIP_DETAIL = RelationshipViewSet.as_view({
    'delete': 'destroy'
})

urlpatterns = [
    path('relationship-api/', RELATIONSHIP_LIST),
    path('relationship-api/<int:id>/', RELATIONSHIP_DETAIL),
]