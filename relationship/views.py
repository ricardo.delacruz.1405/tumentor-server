""" Views for Relationship app """
from django.contrib.auth.models import User
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from relationship.serializers import RelationshipSerializer
from relationship.models import Relationship
from django.http.response import Http404
from rest_framework.response import Response
from rest_framework import status
from core.models import Notification

# Create your views here.
class SmallSetPagination(PageNumberPagination):
    page_size = 10


class MediumSetPagination(PageNumberPagination):
    page_size = 15


class LargeSetPagination(PageNumberPagination):
    page_size = 20


class RelationshipViewSet(ModelViewSet):
    """ Relationship ViewSet """
    serializer_class = RelationshipSerializer
    permission_classes = [AllowAny]
    pagination_class = LargeSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = Relationship.objects.all()
        if 'id_followed' in self.request.GET:
            id_followed = self.request.GET['id_followed']
            if User.objects.filter(id=id_followed).exists():
                queryset = (Relationship.objects
                            .filter(followed_id=id_followed)
                            .order_by('follower__first_name'))
        if 'id_follower' in self.request.GET:
            id_follower = self.request.GET['id_follower']
            if User.objects.filter(id=id_follower).exists():
                queryset = (Relationship.objects
                            .filter(follower_id=id_follower)
                            .order_by('followed__first_name'))
        return queryset

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            notifications = Notification.objects.filter(use_case=1,
                                                        object_id=instance.id)
            if notifications.exists():
                notifications.delete()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)
