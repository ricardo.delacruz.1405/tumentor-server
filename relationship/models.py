""" Models for relationship application """
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Relationship(models.Model):
    """ Relationship model """
    follower = models.ForeignKey(User, on_delete=models.CASCADE,
                                 related_name='follower')
    followed = models.ForeignKey(User, on_delete=models.CASCADE,
                                 related_name='followed')
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return (self.follower.get_full_name() + ' follow: ' +
                self.followed.get_full_name())