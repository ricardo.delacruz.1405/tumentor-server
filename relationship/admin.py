from django.contrib import admin
from relationship.models import Relationship

# Register your models here.
admin.site.register(Relationship)
