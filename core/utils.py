""" Utils for core application """
import json
import requests
from django.contrib.auth.models import User
from django.db.models import Q
from tumentor import settings
from core.models import UserImage, UserDevice

def create_username(first_name, last_name):
    """create username account
        parameters
        ----------
        first_name: String
            first name of user account
        last_name: String
            last name of user account"""
    # There are some names with dots, like: MAKYAURUM E.I.R.L.
    # we remove dots to avoid problems with the algo here that
    # depends on dots to split the root of the username
    first_name = first_name.split(" ")[0].replace(".", "")
    last_name = last_name.split(" ")[0].replace(".", "")
    username = (first_name + "." + last_name).lower()
    usernames = User.objects.filter(username__contains=username).order_by('id')
    if usernames:
        for user_name in usernames:
            repeated_username = user_name.username
            first_part = repeated_username.split('.')[0]
            second_part = repeated_username.split('.')[1]
            if (username.split('.')[0] == first_part and
                    username.split('.')[1] == second_part):
                split_repeated_username = repeated_username.split('.')
                if len(split_repeated_username) == 2:
                    username = username + '.1'
                else:
                    count = int(repeated_username.split('.')[2])
                    count += 1
                    if len(split_repeated_username) == 3:
                        split_repeated_username[2] = str(count)
                        username = '.'.join(split_repeated_username)
                    else:
                        username = username + '.' + str(count)
    return username


def get_image_of_user(user):
    """ Get image of user """
    user_image = UserImage.objects.filter(user=user).order_by('-date')
    if user_image.exists():
        image = settings.IMAGE_HOST + user_image.first().image.url
    else:
        image = settings.IMAGE_HOST + '/static/core/img/profile/user.png'
        if user.userinformation:
            if user.userinformation.gender:
                if int(user.userinformation.gender) == 1:
                    image = (settings.IMAGE_HOST +
                             '/static/core/img/profile/man.png')
                else:
                    image = (settings.IMAGE_HOST +
                             '/static/core/img/profile/woman.png')
    return image


def search_users(to_search):
    """ Method to search users """
    to_search = to_search.strip()
    users = []
    if len(to_search) > 0:
        array_to_search = to_search.split()
        flag = True
        query = None
        for to_search in array_to_search:
            if flag:
                flag = False
                query = (Q(first_name__icontains=to_search) |
                         Q(last_name__icontains=to_search))
            else:
                query = query & (Q(first_name__icontains=to_search) |
                                 Q(last_name__icontains=to_search))
        users = (User.objects.filter(query).filter(is_active=True)
                 .order_by('first_name'))
    return users


def get_user_devices(id_user):
    """ Get user devices """
    devices = []
    user_devices = UserDevice.objects.filter(user_id=id_user)
    if user_devices.exists():
        devices = list(user_devices.values_list('device', flat=True))
    return devices


def send_push_notification(devices, title, content, image=None, data=None):
    """ Send push notifications """
    header = {"Content-Type": "application/json; charset=utf-8"}
    app_id = '3a6977c7-9ab8-4406-a3f7-45f3787a2f0a'
    payload = {"include_player_ids": devices,
               "app_id": app_id,
               "headings": {"en": title},
               "contents": {"en": content},
               "priority": 10,
               "android_led_color": "#00e676",
               "android_accent_color": "#00e676"}
    if image:
        payload["large_icon"] = image
    if data:
        payload["data"] = data
    request = requests.post('https://onesignal.com/api/v1/notifications',
                            headers=header, data=json.dumps(payload))
    return request.status_code
