""" URL's for core application """
from django.urls import path

from core.views import UserInformationViewSet, UserViewSet, LoginAPI, SignupAPI,\
    UserImageViewSet, NotificationViewSet, UserDeviceViewSet
from core.views import EmailAPI

USER_LIST = UserViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
USER_DETAIL = UserViewSet.as_view({
    'get': 'retrieve',
    'patch': 'partial_update'
})
USER_INFORMATION_LIST = UserInformationViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
USER_INFORMATION_DETAIL = UserInformationViewSet.as_view({
    'get': 'retrieve',
    'patch': 'partial_update'
})
USER_IMAGE_LIST = UserImageViewSet.as_view({
    'post': 'create'
})
USER_DEVICE_LIST = UserDeviceViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
USER_DEVICE_DETAIL = UserDeviceViewSet.as_view({
    'delete': 'destroy'
})
NOTIFICATION_LIST = NotificationViewSet.as_view({
    'get': 'list'
})

urlpatterns = [
    path('users-api/', USER_LIST),
    path('user-api/<int:id>/', USER_DETAIL),
    path('users-information-api/', USER_INFORMATION_LIST),
    path('user-information-api/<int:user_id>/', USER_INFORMATION_DETAIL),
    path('login-api/', LoginAPI.as_view()),
    path('signup-api/', SignupAPI.as_view()),
    path('email-api/', EmailAPI.as_view()),
    path('user-image-api/', USER_IMAGE_LIST),
    path('user-device-api/', USER_DEVICE_LIST),
    path('user-device-api/<uuid:device>/', USER_DEVICE_DETAIL),
    path('notification-api/', NOTIFICATION_LIST),
]
