""" Views for Core app """
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.pagination import PageNumberPagination
from core.serializers import UserInformationSerializer, UserSerializer,\
    UserImageSerializer, NotificationSerializer, UserDeviceSerializer
from core.models import UserInformation, UserProfile, UserImage, Notification,\
    UserDevice
from django.contrib.auth.models import User
from rest_auth.models import TokenModel
from rest_auth.serializers import TokenSerializer
from core.utils import create_username, search_users

# Create your views here.
class SmallSetPagination(PageNumberPagination):
    page_size = 10


class MediumSetPagination(PageNumberPagination):
    page_size = 15


class LargeSetPagination(PageNumberPagination):
    page_size = 20


class ExtraLargeSetPagination(PageNumberPagination):
    page_size = 25


class UserViewSet(ModelViewSet):
    """ User ViewSet """
    serializer_class = UserSerializer
    permission_classes = [AllowAny]
    pagination_class = LargeSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = User.objects.filter(is_active=True)
        if 'to_search' in self.request.GET:
            to_search = self.request.GET['to_search']
            queryset = search_users(to_search)
        return queryset
    

class UserInformationViewSet(ModelViewSet):
    """ User Information ViewSet """
    queryset = UserInformation.objects.all()
    serializer_class = UserInformationSerializer
    permission_classes = [AllowAny]
    lookup_field = 'user_id'


class LoginAPI(APIView):
    """ API View for login """
    def post(self, request):
        if 'email' in request.data and 'password' in request.data:
            email = self.request.data['email']
            password = self.request.data['password']
            if User.objects.filter(email=email).exists():
                user = User.objects.filter(email=email).first()
                if user.check_password(password):
                    context = {'request': request}
                    obj_user = UserSerializer(user, context=context).data
                    if TokenModel.objects.filter(user=user).exists():
                        token = TokenModel.objects.filter(user=user).first()
                    else:
                        token = TokenModel.objects.create(user=user)
                    obj_token = TokenSerializer(token).data
                    return Response({'user': obj_user, 'token': obj_token},
                                    status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_204_NO_CONTENT)


class SignupAPI(APIView):
    """ API View for login """
    def post(self, request):
        if ('email' in request.data and 'password' in request.data and
            'gender' in request.data and 'first_name' in request.data 
            and 'last_name' in request.data):
            first_name = self.request.data['first_name']
            last_name = self.request.data['last_name']
            email = self.request.data['email']
            password = self.request.data['password']
            gender = self.request.data['gender']
            if not User.objects.filter(email=email).exists():
                username = create_username(first_name, last_name)
                user = User.objects.create(first_name=first_name,
                                           last_name=last_name,
                                           username=username,
                                           email=email, is_active=True)
                user.set_password(password)
                user.save()
                UserInformation.objects.create(user=user, gender=gender)
                token = TokenModel.objects.create(user=user)
                context = {'request': request}
                obj_user = UserSerializer(user, context=context).data
                obj_token = TokenSerializer(token).data
                return Response({'user': obj_user, 'token': obj_token},
                                status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_204_NO_CONTENT)


class EmailAPI(APIView):
    """ API View for validate if email exists """
    def get(self, request):
        if 'email' in request.GET:
            email = request.GET['email']
            if User.objects.filter(email=email).exists():
                return Response({'exists': True})
        return Response({'exists': False})


class UserImageViewSet(ModelViewSet):
    """ User Image ViewSet """
    serializer_class = UserImageSerializer
    permission_classes = [AllowAny]
    pagination_class = MediumSetPagination

    def get_queryset(self):
        queryset = UserImage.objects.all()
        return queryset


class UserDeviceViewSet(ModelViewSet):
    """ User Device ViewSet """
    serializer_class = UserDeviceSerializer
    permission_classes = [AllowAny]
    lookup_field = 'device'

    def get_queryset(self):
        queryset = UserDevice.objects.all().order_by('date')
        if 'device' in self.request.GET:
            device = self.request.GET['device']
            queryset = (UserDevice.objects.filter(device=device)
                        .order_by('date'))
        return queryset


class NotificationViewSet(ModelViewSet):
    """ Notification ViewSet """
    serializer_class = NotificationSerializer
    permission_classes = [AllowAny]
    pagination_class = ExtraLargeSetPagination

    def get_queryset(self):
        queryset = Notification.objects.all().order_by('-date')
        if 'id_user' in self.request.GET:
            id_user = self.request.GET['id_user']
            try:
                id_user = int(id_user)
                if User.objects.filter(id=id_user).exists():
                    queryset = (Notification.objects.filter(user_id=id_user)
                                .order_by('-date'))
            except ValueError:
                queryset = []
                print(ValueError)
        return queryset
