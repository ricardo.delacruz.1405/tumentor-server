''' Admin for core application '''
from django.contrib import admin
from core.models import UserInformation, UserProfile, UserImage, UserImageType,\
    Notification, UserDevice, Location

# Register your models here.
admin.site.register(UserInformation)
admin.site.register(UserProfile)
admin.site.register(UserImageType)
admin.site.register(UserImage)
admin.site.register(UserDevice)
admin.site.register(Notification)
admin.site.register(Location)
