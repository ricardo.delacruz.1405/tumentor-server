""" Serializers for core application """
from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from rest_framework.fields import SerializerMethodField
from core.models import UserInformation, UserImage, UserImageType, Notification, \
    UserDevice, Location
from core.utils import get_image_of_user
from relationship.models import Relationship
from post.models import Post, PostLike, PostComment, PostCommentLike


class BasicUserSerializer(ModelSerializer):
    """ Basic User Serializer """
    image = SerializerMethodField()
    relationship = SerializerMethodField()
    
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'image', 'relationship']

    def get_image(self, obj):
        """ Get image of user """
        image = get_image_of_user(obj)
        return image

    def get_relationship(self, obj):
        """ Know if logged user follow user """
        relationship = {'id': None, 'is_following': False}
        if 'id_logged_user' in self.context['request'].GET:
            id_logged_user = self.context['request'].GET['id_logged_user']
            if User.objects.filter(id=id_logged_user).exists():
                if Relationship.objects.filter(follower_id=id_logged_user,
                                               followed_id=obj.id).exists():
                    obj_relationship = (Relationship.objects
                                        .filter(follower_id=id_logged_user,
                                                followed_id=obj.id).first())
                    relationship = {'id': obj_relationship.id,
                                    'is_following': True}
        return relationship


class UserInformationSerializer(ModelSerializer):
    """ User Information Serializer """
    #obj_user = SerializerMethodField()

    class Meta:
        model = UserInformation
        fields = ['id', 'description']

    """
    def get_obj_user(self, obj):
        user = None
        context = {'request': self.context['request']}
        if obj.user:
            user = UserSerializer(obj.user, context=context).data
        return user
    """


class UserSerializer(ModelSerializer):
    """ User Serializer """
    image = SerializerMethodField()
    num_followers = SerializerMethodField()
    num_followeds = SerializerMethodField()
    num_posts = SerializerMethodField()
    relationship = SerializerMethodField()
    user_information = SerializerMethodField()
    
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'image', 'num_followers',
                  'relationship', 'num_followeds', 'num_posts',
                  'user_information']

    def get_image(self, obj):
        """ Get image of user """
        image = get_image_of_user(obj)
        return image

    def get_num_followers(self, obj):
        """ Get num followers of user """
        num_followers = Relationship.objects.filter(followed=obj).count()
        return num_followers

    def get_num_followeds(self, obj):
        """ Get num followeds of user """
        num_followeds = Relationship.objects.filter(follower=obj).count()
        return num_followeds

    def get_num_posts(self, obj):
        """ Get num posts of user """
        num_posts = Post.objects.filter(writer=obj).count()
        return num_posts

    def get_relationship(self, obj):
        """ Know if logged user follow user """
        relationship = {'id': None, 'is_following': False}
        if 'id_logged_user' in self.context['request'].GET:
            id_logged_user = self.context['request'].GET['id_logged_user']
            if User.objects.filter(id=id_logged_user).exists():
                if Relationship.objects.filter(follower_id=id_logged_user,
                                               followed_id=obj.id).exists():
                    obj_relationship = (Relationship.objects
                                        .filter(follower_id=id_logged_user,
                                                followed_id=obj.id).first())
                    relationship = {'id': obj_relationship.id,
                                    'is_following': True}
        return relationship

    def get_user_information(self, obj):
        user_information = None
        if obj.userinformation:
            context = {'request': self.context['request']}
            user_information = UserInformationSerializer(obj.userinformation,
                                                         context=context).data
        return user_information


class UserImageSerializer(ModelSerializer):
    """ User Image Serializer """
    from post.serializers import CustomBase64FileField
    image = CustomBase64FileField()

    class Meta:
        """ Meta Class  """
        model = UserImage
        fields = ['id', 'user', 'image']

    def create(self, validated_data):
        user_image = UserImage.objects.create(**validated_data)
        if 'image_type' in self.context['request'].data:
            image_type = self.context['request'].data['image_type']
            user_image_type = UserImageType.objects.filter(type=image_type)
            if user_image_type.exists():
                user_image_type = user_image_type.first()
                user_image.type = user_image_type
                user_image.save()
        return user_image


class UserDeviceSerializer(ModelSerializer):
    """ User Device Serializer """
    class Meta:
        """ Meta Class  """
        model = UserDevice
        fields = ['id', 'user', 'device']


class NotificationSerializer(ModelSerializer):
    """ Notification Serializer """
    object_data = SerializerMethodField()

    class Meta:
        """ Meta Class """
        model = Notification
        fields = ['id', 'use_case', 'date', 'object_data']

    def get_object_data(self, obj):
        from relationship.serializers import RelationshipSerializer
        from post.serializers import PostLikeSerializer, PostCommentSerializer
        from post.serializers import PostCommentLikeSerializer
        object_data = None
        context = {'request': self.context['request']}
        if obj.use_case == 1:
            relationship = Relationship.objects.filter(id=obj.object_id)
            if relationship.exists():
                relationship = relationship.first()
                object_data = RelationshipSerializer(relationship,
                                                     context=context).data
        elif obj.use_case == 2:
            post_like = PostLike.objects.filter(id=obj.object_id)
            if post_like.exists():
                post_like = post_like.first()
                object_data = PostLikeSerializer(post_like,
                                                 context=context).data
        elif obj.use_case == 3:
            post_comment = PostComment.objects.filter(id=obj.object_id)
            if post_comment.exists():
                post_comment = post_comment.first()
                object_data = PostCommentSerializer(post_comment,
                                                    context=context).data
        elif obj.use_case == 4:
            comment_like = PostCommentLike.objects.filter(id=obj.object_id)
            if comment_like.exists():
                comment_like = comment_like.first()
                object_data = PostCommentLikeSerializer(comment_like,
                                                        context=context).data
        return object_data


class LocationSerializer(ModelSerializer):
    """ Location Serializer """

    class Meta:
        """ Meta Class """
        model = Location
        fields = ['address', 'lat', 'lng']
