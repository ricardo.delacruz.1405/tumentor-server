# Generated by Django 2.0.6 on 2019-06-05 22:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_notification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='use_case',
            field=models.PositiveSmallIntegerField(choices=[(1, 'New Follower'), (2, 'New Post Like')]),
        ),
    ]
