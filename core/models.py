""" Models for core application """
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class UserProfile(models.Model):
    """ User Profile Model """
    DISCIPLE = 1
    MENTOR = 2
    VALUE_CHOICES = (
        (DISCIPLE, 'disciple'),
        (MENTOR, 'mentor')
    )
    
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    value = models.PositiveSmallIntegerField(choices=VALUE_CHOICES)

    def __str__(self):
        return self.name


class UserInformation(models.Model):
    """ User Information Model """
    MALE = 1
    FEMALE = 2
    GENDER_CHOICES = (
        (MALE, 'male'),
        (FEMALE, 'female')
    )
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES,
                                              blank=True, null=True)

    def __str__(self):
        if self.gender:
            gender = 'MALE'
            if self.gender == 2:
                gender = 'FEMALE'
            return self.user.get_full_name() + ' (' + gender + ')'
        return self.user.get_full_name()


class UserImageType(models.Model):
    """ User Image Type model """
    PROFILE = 1
    TYPE_CHOICES = (
        (PROFILE, 'Profile'),
    )

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES,
                                            blank=True, null=True)

    def __str__(self):
        return self.name


class UserImage(models.Model):
    """ User Image model """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.ForeignKey(UserImageType, on_delete=models.CASCADE,
                             blank=True, null=True)
    image = models.ImageField(upload_to='user_images/%Y/%m/%d')
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user.get_full_name() + ' (' + self.image.url + ')'


class UserDevice(models.Model):
    """ User Device model """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    device = models.CharField(max_length=100)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user.get_full_name() + ' (' + self.device + ')'


class Notification(models.Model):
    """ Notification model """
    NEW_FOLLOWER = 1
    NEW_POST_LIKE = 2
    NEW_POST_COMMENT = 3
    NEW_COMMENT_LIKE = 4
    USE_CASE_CHOICES = (
        (NEW_FOLLOWER, 'New Follower'),
        (NEW_POST_LIKE, 'New Post Like'),
        (NEW_POST_COMMENT, 'New Post Comment'),
        (NEW_COMMENT_LIKE, 'New Comment Like'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    use_case = models.PositiveSmallIntegerField(choices=USE_CASE_CHOICES)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        use_case = 'NO USE CASE'
        if self.use_case == 1:
            use_case = 'New Follower'
        elif self.use_case == 2:
            use_case = 'New Post Like'
        elif self.use_case == 3:
            use_case = 'New Post Comment'
        elif self.use_case == 4:
            use_case = 'New Comment Like'
        return ('(' + use_case + ') ' + self.user.get_full_name() +
                ' - (' + str(self.object_id) + ')')


class Location(models.Model):
    """ Location model """
    address = models.CharField(max_length=200)
    google_address = models.CharField(max_length=200, blank=True, null=True)
    lat = models.CharField(max_length=60, blank=True, null=True)
    lng = models.CharField(max_length=60, blank=True, null=True)

    def __str__(self):
        address = self.address
        if self.lat and self.lng:
            address += ' (' + self.lat + ', ' + self.lng + ')'
        return address
