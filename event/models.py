from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from core.models import Location

# Create your models here.
class Event(models.Model):
    """ Event model """
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    location = models.ForeignKey(Location, on_delete=models.SET_NULL,
                                 blank=True, null=True)
    name = models.CharField(max_length=150)
    description = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to='appointments/%Y/%m/%d',
                              blank=True, null=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(blank=True, null=True)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name + ' created by: ' + self.creator.get_full_name()
