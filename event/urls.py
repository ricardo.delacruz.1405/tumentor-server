""" URL's for event application """
from django.urls import path
from event.views import EventViewSet

EVENT_LIST = EventViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

urlpatterns = [
    path('event-api/', EVENT_LIST),
]
