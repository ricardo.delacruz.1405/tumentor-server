""" Views for event application """
from rest_framework.viewsets import ModelViewSet
from event.serializers import EventSerializer
from rest_framework.permissions import AllowAny
from event.models import Event
from django.contrib.auth.models import User
from rest_framework.pagination import PageNumberPagination

# Create your views here.
class SmallSetPagination(PageNumberPagination):
    page_size = 10


class MediumSetPagination(PageNumberPagination):
    page_size = 15


class LargeSetPagination(PageNumberPagination):
    page_size = 20


class EventViewSet(ModelViewSet):
    """ Post ViewSet """
    serializer_class = EventSerializer
    permission_classes = [AllowAny]
    pagination_class = MediumSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = Event.objects.all().order_by('-date')
        if 'id_user' in self.request.GET:
            id_user = self.request.GET['id_user']
            try:
                id_user = int(id_user)
                if User.objects.filter(id=id_user).exists():
                    queryset = (Event.objects.filter(creator_id=id_user)
                                .order_by('-date'))
            except ValueError:
                print(ValueError)
        return queryset