""" Serializers for event application """
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from core.serializers import LocationSerializer
from event.models import Event
from core.models import Location

class EventSerializer(ModelSerializer):
    """ Post Serializer """
    obj_location = SerializerMethodField()

    class Meta:
        """ Meta class """
        model = Event
        fields = ['id', 'creator', 'name', 'description', 'start_date',
                  'end_date', 'obj_location']

    def create(self, validated_data):
        """ Create method """
        event = Event.objects.create(**validated_data)
        if 'location' in self.context['request'].data:
            location = self.context['request'].data['location']
            obj_location = (Location.objects
                            .create(lat=location['lat'],
                                    lng=location['lng'],
                                    address=location['address'],
                                    google_address=location['address']))
            event.location = obj_location
            event.save()
        return event

    def get_obj_location(self, obj):
        """ Get location of event """
        obj_location = None
        if obj.location:
            context = {'request': self.context['request']}
            obj_location = LocationSerializer(obj.location, context=context).data
        return obj_location
