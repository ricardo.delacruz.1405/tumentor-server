""" URL's for post application """
from django.urls import path, re_path
from post.views import PostViewSet, PostLikeViewSet, PostFileViewSet,\
    PostCommentViewSet, PostCommentLikeViewSet

POST_LIST = PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
POST_DETAIL = PostViewSet.as_view({
    'get': 'retrieve'
})
POST_FILE_LIST = PostFileViewSet.as_view({
    'post': 'create'
})
POST_LIKE_LIST = PostLikeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
POST_LIKE_DETAIL = PostLikeViewSet.as_view({
    'delete': 'destroy'
})
POST_COMMENT_LIST = PostCommentViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
POST_COMMENT_LIKE_LIST = PostCommentLikeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
POST_COMMENT_LIKE_DETAIL = PostCommentLikeViewSet.as_view({
    'delete': 'destroy'
})

urlpatterns = [
    path('post-api/', POST_LIST),
    path('post-api/<int:id>/', POST_DETAIL),
    path('post-file-api/', POST_FILE_LIST),
    path('post-like-api/', POST_LIKE_LIST),
    path('post-like-api/<int:id>/', POST_LIKE_DETAIL),
    path('post-comment-api/', POST_COMMENT_LIST),
    path('post-comment-like-api/', POST_COMMENT_LIKE_LIST),
    path('post-comment-like-api/<int:id>/', POST_COMMENT_LIKE_DETAIL),
]
