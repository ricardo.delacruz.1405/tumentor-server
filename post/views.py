from django.contrib.auth.models import User
from rest_framework.pagination import PageNumberPagination
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from post.serializers import PostSerializer, PostLikeSerializer,\
    PostFileSerializer, PostCommentSerializer, PostCommentLikeSerializer
from post.models import Post, PostLike, PostFile, PostComment, PostCommentLike
from relationship.models import Relationship
from django.http.response import Http404
from rest_framework.response import Response
from rest_framework import status
from core.models import Notification

# Create your views here.
class SmallSetPagination(PageNumberPagination):
    page_size = 10


class MediumSetPagination(PageNumberPagination):
    page_size = 15


class LargeSetPagination(PageNumberPagination):
    page_size = 20


class PostViewSet(ModelViewSet):
    """ Post ViewSet """
    serializer_class = PostSerializer
    permission_classes = [AllowAny]
    pagination_class = MediumSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = Post.objects.all().order_by('-date')
        if 'own_posts' in self.request.GET:
            if 'id_user' in self.request.GET:
                id_user = self.request.GET['id_user']
                if User.objects.filter(id=id_user).exists():
                        queryset = (Post.objects.filter(writer_id=id_user)
                                    .order_by('-date'))
        else:
            if 'id_user_newfeeds' in self.request.GET:
                id_user_newfeeds = self.request.GET['id_user_newfeeds']
                if User.objects.filter(id=id_user_newfeeds).exists():
                    followeds = (Relationship.objects
                                 .filter(follower_id=id_user_newfeeds)
                                 .values('followed_id'))
                    queryset = (Post.objects.filter(writer_id__in=followeds)
                                .order_by('-date'))
        return queryset


class PostFileViewSet(ModelViewSet):
    """ Post File ViewSet """
    serializer_class = PostFileSerializer
    permission_classes = [AllowAny]
    pagination_class = SmallSetPagination

    def get_queryset(self):
        queryset = PostFile.objects.all()
        return queryset


class PostLikeViewSet(ModelViewSet):
    """ Post Like ViewSet """
    serializer_class = PostLikeSerializer
    permission_classes = [AllowAny]
    pagination_class = LargeSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = PostLike.objects.all().order_by('-date')
        if 'id_post' in self.request.GET:
            id_post = self.request.GET['id_post']
            if Post.objects.filter(id=id_post).exists():
                queryset = (PostLike.objects.filter(post_id=id_post)
                            .order_by('-date'))
        return queryset

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            notifications = Notification.objects.filter(use_case=2,
                                                        object_id=instance.id)
            if notifications.exists():
                notifications.delete()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class PostCommentViewSet(ModelViewSet):
    """ Post Comment ViewSet """
    serializer_class = PostCommentSerializer
    permission_classes = [AllowAny]
    pagination_class = MediumSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = PostComment.objects.all().order_by('-date')
        if 'id_post' in self.request.GET:
            id_post = self.request.GET['id_post']
            if Post.objects.filter(id=id_post).exists():
                queryset = (PostComment.objects.filter(post_id=id_post)
                            .order_by('-date'))
        return queryset

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            notifications = Notification.objects.filter(use_case=3,
                                                        object_id=instance.id)
            if notifications.exists():
                notifications.delete()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class PostCommentLikeViewSet(ModelViewSet):
    """ Post Comment Like ViewSet """
    serializer_class = PostCommentLikeSerializer
    permission_classes = [AllowAny]
    pagination_class = LargeSetPagination
    lookup_field = 'id'

    def get_queryset(self):
        queryset = PostCommentLike.objects.all().order_by('-date')
        if 'id_post_comment' in self.request.GET:
            id_post_comment = self.request.GET['id_post_comment']
            if PostComment.objects.filter(id=id_post_comment).exists():
                queryset = (PostCommentLike.objects
                            .filter(post_comment_id=id_post_comment)
                            .order_by('-date'))
        return queryset

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            notifications = Notification.objects.filter(use_case=4,
                                                        object_id=instance.id)
            if notifications.exists():
                notifications.delete()
            self.perform_destroy(instance)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)
