from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.
class Post(models.Model):
    """ Post model """
    writer = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField(blank=True, null=True)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        content = ''
        if self.content:
            content = self.content
        return self.writer.get_full_name() + ' - ' + content


class PostFile(models.Model):
    """ Post File model """
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    file = models.FileField(upload_to='post_files/%Y/%m/%d')
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return 'Post (' + str(self.post.id) + ') - ' + self.file.url


class PostLike(models.Model):
    """ Post Like Model """
    post = models.ForeignKey(Post, on_delete=models.CASCADE,
                             related_name='likes')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return (self.user.get_full_name() + ' - ' + str(self.post.id))


class PostComment(models.Model):
    """ Post Comment Model """
    post = models.ForeignKey(Post, on_delete=models.CASCADE,
                             related_name='comments')
    writer = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    is_public = models.BooleanField(default=True)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return ('Post (' + str(self.post.id) + ') - ' +
                self.writer.get_full_name() + ': ' + self.content)


class PostCommentLike(models.Model):
    """ Post Comment Like Model """
    post_comment = models.ForeignKey(PostComment, on_delete=models.CASCADE,
                                     related_name='likes')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return (self.user.get_full_name() + ' - ' + str(self.post_comment.id))
    
    