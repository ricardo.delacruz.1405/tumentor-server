""" Serializers for post application """
import base64
import uuid
from mimetypes import guess_extension
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.utils import six
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from rest_framework.serializers import ModelSerializer
from post.models import Post, PostComment, PostCommentLike, PostLike, PostFile
from rest_framework.fields import SerializerMethodField, FileField
from core.serializers import BasicUserSerializer
from tumentor import settings
from tumentor.settings import IMAGE_HOST
from core.models import Notification
from core.utils import get_user_devices, get_image_of_user,\
    send_push_notification


class CustomBase64FileField(FileField):
    """class to get the file from base64 string sent to server in json format"""
    INVALID_FILE_MESSAGE = "Please upload a valid file."
    INVALID_TYPE_MESSAGE = "The type of the file couldn't be determined"
    ALLOWED_TYPES = ['.pdf', '.doc', '.docx', '.ppt', '.pptx', '.xls', '.xlsx',
                     '.csv', '.png', '.jpg', '.jpeg', '.txt', '.gif', '.mp3',
                     '.mp4', '.avi', '.3gp']

    def to_internal_value(self, data):
        if not data or data is None or data == '':
            return None
        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')
                x,file_type = header.split('data:') 
            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except (TypeError):
                raise ValidationError(self.INVALID_FILE_MESSAGE)
            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file,
                                                     file_type)
            if file_type == 'audio/mp3':
                file_extension = '.mp3'
            if file_extension == '.jpe':
                file_extension = '.jpg'
            if not file_extension:
                file_extension = '.dcm'
            if file_extension not in self.ALLOWED_TYPES:
                raise ValidationError(self.INVALID_TYPE_MESSAGE)
            complete_file_name = "%s%s" % (file_name, file_extension, )
            data = ContentFile(decoded_file, name=complete_file_name)
        return super(CustomBase64FileField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file, file_type):
        extension = guess_extension(file_type)
        return extension


class PostSerializer(ModelSerializer):
    """ Post Serializer """
    obj_writer = SerializerMethodField()
    likes = SerializerMethodField()
    num_comments = SerializerMethodField()
    files = SerializerMethodField()

    class Meta:
        """ Meta class """
        model = Post
        fields = ['id', 'writer', 'obj_writer', 'content', 'date', 'likes',
                  'num_comments', 'files']

    def create(self, validated_data):
        """ Create method """
        post = Post.objects.create(**validated_data)
        return post

    def get_obj_writer(self, obj):
        """ Serialized writer """
        context = {'request': self.context['request']}
        obj_writer = BasicUserSerializer(obj.writer, context=context).data
        return obj_writer

    def get_likes(self, obj):
        """ Likes of post """
        like_it = False
        if 'id_logged_user' in self.context['request'].GET:
            id_logged_user = self.context['request'].GET['id_logged_user']
            if User.objects.filter(id=id_logged_user).exists():
                if (PostLike.objects.filter(post=obj,
                                            user_id=id_logged_user).exists()):
                    like_it = True
        num_likes = obj.likes.count()
        return {'like_it': like_it, 'quantity': num_likes}

    def get_num_comments(self, obj):
        """ Comments of post """
        return obj.comments.count()

    def get_files(self, obj):
        """ Files of post """
        files = []
        if PostFile.objects.filter(post=obj).exists():
            post_files = PostFile.objects.filter(post=obj)
            for post_file in post_files:
                url_post_file = settings.IMAGE_HOST + post_file.file.url
                files.append({'id': post_file.id,
                              'url': url_post_file})
        return files


class PostLikeSerializer(ModelSerializer):
    """ Post Like Serializer """
    obj_user = SerializerMethodField()

    class Meta:
        """ Meta Class  """
        model = PostLike
        fields = ['id', 'post', 'user', 'obj_user']

    def create(self, validated_data):
        data = self.context['request'].data
        if 'customize_id' in data:
            customize_id = data['customize_id']
            post = validated_data['post']
            user = validated_data['user']
            post_like = (PostLike.objects
                         .create(id=customize_id, user=user, post=post))
        else:
            post_like = PostLike.objects.create(**validated_data)
        if post_like.post.writer.id != post_like.user.id:
            # Notification New Post Like
            Notification.objects.create(user=post_like.post.writer,
                                        object_id=post_like.id, use_case=2)
            # Push notification
            devices = get_user_devices(post_like.post.writer.id)
            title = 'TuMentor'
            content = ('A ' + post_like.user.get_full_name() +
                       ' le ha gustado una de tus publicaciones')
            image = get_image_of_user(post_like.user)
            push_data = {"use_case": 2, "id_post": post_like.post.id}
            send_push_notification(devices, title, content, image, push_data)
        return post_like

    def get_obj_user(self, obj):
        """ Serialized user """
        context = {'request': self.context['request']}
        obj_user = BasicUserSerializer(obj.user, context=context).data
        return obj_user


class PostCommentSerializer(ModelSerializer):
    """ Post Comment Serializer """
    obj_writer = SerializerMethodField()
    likes = SerializerMethodField()

    class Meta:
        """ Meta Class  """
        model = PostComment
        fields = ['id', 'content', 'date', 'post', 'obj_writer', 'likes',
                  'writer']

    def create(self, validated_data):
        post_comment = PostComment.objects.create(**validated_data)
        if post_comment.post.writer.id != post_comment.writer.id:
            # Notification New Post Comment
            Notification.objects.create(user=post_comment.post.writer,
                                        object_id=post_comment.id, use_case=3)
            # Push notification
            devices = get_user_devices(post_comment.post.writer.id)
            title = 'TuMentor'
            content = (post_comment.writer.get_full_name() +
                       ' ha comentado una de tus publicaciones')
            image = get_image_of_user(post_comment.writer)
            push_data = {"use_case": 3, "id_post": post_comment.post.id}
            send_push_notification(devices, title, content, image, push_data)
        return post_comment

    def get_obj_writer(self, obj):
        """ Get serialized solver """
        obj_writer = None
        if obj.writer:
            context = {'request': self.context['request']}
            obj_writer = BasicUserSerializer(obj.writer, context=context).data
        return obj_writer

    def get_likes(self, obj):
        """ This user like the answer """
        like_it = False
        if 'id_logged_user' in self.context['request'].GET:
            id_logged_user = self.context['request'].GET['id_logged_user']
            if User.objects.filter(id=id_logged_user).exists():
                if (PostCommentLike.objects
                    .filter(post_comment=obj,
                            user_id=id_logged_user).exists()):
                    like_it = True
        num_likes = obj.likes.count()
        return {'like_it': like_it, 'quantity': num_likes}


class PostCommentLikeSerializer(ModelSerializer):
    """ Post Comment Like Serializer """
    obj_user = SerializerMethodField()
    id_post = SerializerMethodField()

    class Meta:
        """ Meta Class  """
        model = PostCommentLike
        fields = ['id', 'post_comment', 'user', 'obj_user', 'id_post']

    def create(self, validated_data):
        if 'customize_id' in self.context['request'].data:
            customize_id = self.context['request'].data['customize_id']
            post_comment = validated_data['post_comment']
            user = validated_data['user']
            post_comment_like = (PostCommentLike.objects
                                 .create(id=customize_id, user=user,
                                         post_comment=post_comment))
        else:
            post_comment_like = (PostCommentLike.objects
                                 .create(**validated_data))
        comment_writer = post_comment_like.post_comment.writer
        if comment_writer.id != post_comment_like.user.id:
            # Notification New Comment Like
            Notification.objects.create(user=comment_writer,
                                        object_id=post_comment_like.id,
                                        use_case=4)
            # Push notification
            devices = get_user_devices(comment_writer.id)
            title = 'TuMentor'
            content = ('A ' + post_comment_like.user.get_full_name() +
                       ' le ha gustado uno de tus comentarios')
            image = get_image_of_user(post_comment_like.user)
            push_data = {"use_case": 4,
                         "id_post": post_comment_like.post_comment.post.id}
            send_push_notification(devices, title, content, image, push_data)
        return post_comment_like

    def get_obj_user(self, obj):
        """ Serialized user """
        context = {'request': self.context['request']}
        obj_user = BasicUserSerializer(obj.user, context=context).data
        return obj_user

    def get_id_post(self, obj):
        """ Id Post """
        return obj.post_comment.post.id


class PostFileSerializer(ModelSerializer):
    """ Post File Serializer """
    file = CustomBase64FileField()

    class Meta:
        """ Meta Class  """
        model = PostFile
        fields = ['id', 'post', 'file']
